#include <stdio.h>
#include <string.h>
#include <stdlib.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


#define MAX_SIZE 1024

void echo_handler(char* argv[]){
	int i = 0;
	int flag = 0;
	char * content;
	char * address;
	while (argv[i]){
		if (flag == 1){ //append
			FILE * fp;
			fp = fopen(argv[i], "a");
			fprintf(fp,"%s" ,content);
			fclose(fp);
		} 
		if (flag == 2){ //redirect
			FILE * fp;
			fp = fopen(argv[i], "w");
			fprintf(fp, "%s", content);
			fclose(fp);
		}
		if (argv[i][0] == '"' || i == 1){
			if (argv[i][0] != '"'){
				size_t len = strlen(argv[i]);
				content = malloc(len);
				for (int j = 0; j < strlen(argv[i]); j++){
					content[j] = argv[i][j];
				}
				content[len] = '\0';
			} else {
				size_t len = strlen(argv[i]) - 2;
				content = malloc(len);
				for (int j = 1; j < strlen(argv[i]); j++){
					content[j-1] = argv[i][j];
				}
				content[len] = '\0';
			}
		} else {
			if (strcmp(argv[i], ">>") == 0){
				flag = 1;
			} else{
				if (strcmp(argv[i], ">") == 0){
					flag = 2;
				}
			}
		
		}
		i++;
	}
	if (flag == 0){
		printf("%s", content);
	}
}


void tokenizer(char *inp){
	char * token;
	char * tks [1024];
	int i = 0;
	token = strtok(inp, " \n");
	while (token != NULL){
		tks[i] = token;
		//printf("%s-", token);
		token = strtok(NULL, " \n");
		i++;
	}
	tks[i] = NULL;
	if (strcmp(tks[0], "echo") != 0){
		pid_t child_pid;
		child_pid = fork();
		if (child_pid == 0){
			execvp(tks[0], tks);
		} else {
			wait(NULL);
		}
	} else {
		echo_handler(tks);
	}
}

int main(int argc, char* argv[]){
	char *buffer;
	size_t buffer_size = MAX_SIZE;
	size_t inp_size;
	buffer = (char *) malloc(buffer_size * sizeof(char));
	char hostname[1024];
	char *username;
    	gethostname(hostname, 1024);
    	username = getenv("USER");
	printf("%s@%s:$", username, hostname);
	inp_size = getline(&buffer, &buffer_size, stdin);
	while (strcmp(buffer, "exit\n") != 0){
		if (strcmp(buffer, "\n") != 0)
			tokenizer(buffer);
		gethostname(hostname, 1024);
    		username = getenv("USER");
    		printf("%s@%s:$", username, hostname);
		inp_size = getline(&buffer, &buffer_size, stdin);
	}
	return 0;
}
