//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////													//////
//////													//////
//////	this program will trace changing in virtual memory by tracing task_struct (PCB) of process	//////
//////													//////
//////													//////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////





#include <linux/module.h>       // Needed by all modules
#include <linux/kernel.h>       // KERN_INFO
#include <linux/sched.h>        // for_each_process, pr_info
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/kthread.h>
#include <linux/delay.h>

struct task_struct *thread;
// thread input argument
int data;
// thread stop return arguments 
int ret;
// flag to stop the thread
int stop;
// array to trace changing in virtual memory
unsigned long int prev[70000];

int thread_function(void *data)
{
	int i;
	for(i = 0 ; i < 70000; i++) prev[i] = 0;

	while(!stop){
		struct task_struct* task;
		int flag = 0;
       		for_each_process(task) {
               	        if(task->mm){
				if(task->mm->total_vm != prev[task->pid]){
					if(flag){
						long int diff = task->mm->total_vm - prev[task->pid];
						if(diff > 0){
							printk(KERN_INFO "%lu \t ****PAGES**** allocated for process \t#%d\n", diff, task->pid);
						}else{
							printk(KERN_INFO "%lu \t ****PAGES**** freed by process \t#%d\n", (-1) * diff, task->pid);
						}
					}
					prev[task->pid] = task->mm->total_vm;
				}else{}
				flag = 1;
        	        }
	    	}
		ssleep(1);
	}
        return 0;
}

int init_module(void)
{
	printk(KERN_INFO "[ INIT ==\n");
	stop = 0;
	thread = kthread_create(&thread_function,NULL,"smjfas");
        thread = kthread_run(&thread_function,NULL,"smjfas");
        return 0;
}

void cleanup_module(void)
{
	stop = 1;
	printk(KERN_INFO "GAME OVER:)!");
	ret = kthread_stop(thread);
}
